// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app_sonar = angular.module('app_sonar', ['ionic']);

app_sonar.run(function($ionicPlatform)
{
  $ionicPlatform.ready(function()
  {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs).
    // The reason we default this to hidden is that native apps don't usually show an accessory bar, at
    // least on iOS. It's a dead giveaway that an app is using a Web View. However, it's sometimes
    // useful especially with forms, though we would prefer giving the user a little more room
    // to interact with the app.
    if(window.cordova && window.Keyboard)
    {
      window.Keyboard.hideKeyboardAccessoryBar(true);
    }

    if(window.StatusBar)
    {
      // Set the statusbar to use the default style, tweak this to
      // remove the status bar on iOS or change it to use white instead of dark colors.
      StatusBar.styleDefault();
    }
  });
})

// app_sonar.config(function($ionicConfigProvider) 
// {
//   $ionicConfigProvider.backButton.previousTitleText(false).text('');
// });

app_sonar.run(function($rootScope, $state)
{
    $rootScope.goBack = function() 
    {
      $rootScope.showCustomBack = false;
      //$state.go("tabs.listdevice");
      //$state.go('app.list', null, {reload: true});
      window.history.back();
      // $ionicHistory.goBack();
    }

    $rootScope.$on('$stateChangeSuccess', function ()
    {
      if($state.$current == 'device')
      {
        $rootScope.showCustomBack = true;
      }
    });
})

app_sonar.controller('ctrl_heartbeat', function ($scope)
{   
    $scope.props = 
    {
      seconds: 10,
      fps: 30
    };

    $scope.do_test = function()
    {
    }
});


app_sonar.controller("ctrl_tabs", function($scope)
{
  $scope.hide_tabs = {"home" : false, "scanner" : false, "heartbeat": false, "about" : false, "listdevice" : true, "listinteraction": true};
  
  function reset()
  {
    for(page in $scope.hide_tabs)
    {
      $scope.hide_tabs[page] = true;
    }
  }

  $scope.$on("$ionicView.enter", function(event, view_data)
  {
    reset();
    page = view_data.stateName;
    
    switch(page)
    {
      case "tabs.home":
        $scope.hide_tabs["home"] = false;
        $scope.hide_tabs["scanner"] = false;
        $scope.hide_tabs["heartbeat"] = false;
        $scope.hide_tabs["about"] = false;
        break;
      case "tabs.scanner":
        $scope.hide_tabs["home"] = false;
        $scope.hide_tabs["scanner"] = false;
        $scope.hide_tabs["listdevice"] = false;
        $scope.hide_tabs["listinteraction"] = false;
        break;
      case "tabs.heartbeat":
        $scope.hide_tabs["home"] = false;
        $scope.hide_tabs["heartbeat"] = false;
        break;
      case "tabs.about":
        $scope.hide_tabs["home"] = false;
        $scope.hide_tabs["about"] = false;
        break;
      case "tabs.listdevice":
        $scope.hide_main_tabs = false;
        $scope.hide_tabs["home"] = false;
        $scope.hide_tabs["scanner"] = false;
        $scope.hide_tabs["listdevice"] = false;
        break;
      case "tabs.listinteraction":
        $scope.hide_tabs["home"] = false;
        $scope.hide_tabs["scanner"] = false;
        $scope.hide_tabs["listinteraction"] = false;
        break;
      default:
    }
  });

});

app_sonar.controller("ctrl_list_device", function($scope)
{
  scope_list_device = $scope;
  $scope.list_device_sonar = [];
  $scope.list_device_normal = [];
  $scope.show_send = false;
  $scope.active_scan = 0;

  $scope.scan_button = function()
  {
    if($scope.active_scan == 0)
    {
      //start scan
      console.log("start scan");     
      $scope.active_scan = 1;
      $scope.show_send = false;
      
      timestamp_start = Math.floor(Date.now() / 1000);
      exec(logging_device, error_logging, 'BLE', 'startScan', []);
    }
    else
    {
      //stop scan
      console.log("stop scan");
      $scope.active_scan = 0;
      
      if($scope.list_device_sonar.length + $scope.list_device_normal.length > 0)
      {
        $scope.show_send = true;
      }
      
      write_csv(filename_csv + i_db, list_proximity);
    }

    active_scan = $scope.active_scan;
  }

  $scope.send_button = function()
  {
    console.log("send");
    send_data();
    $scope.show_send = false;
    $scope.list_device = [];
  }
});

app_sonar.controller("ctrl_device", function($scope, $stateParams, $ionicHistory)
{
  console.log("CTRL DEVICE " + $stateParams.address);
  $scope.device = {};
  $scope.device.address = $stateParams.address;
  //$scope.device.name = device_information[$scope.device.address]["name"];

      // $scope.device.proximity = [];
      //   features = {};
      // features.num = 1;
      // features.rssi = 1;
      // features.distance = 1;
      // features.nb_detection = 1;
      // features.duration = 1;
      // features.date_start = Date.now();
      
      // $scope.device.proximity.push(features);

  if($scope.device.address in list_proximity)
  {
    $scope.device.proximity = [];
    
    for(i in list_proximity[$scope.device.address])
    {
      features = {};
      features.num = parseInt(i) + 1; 
      features.rssi = mean(list_proximity[$scope.device.address][i][2]);
      features.distance = mean(list_proximity[$scope.device.address][i][3]);
      features.nb_detection = list_proximity[$scope.device.address][i][4];
      features.duration = list_proximity[$scope.device.address][i][1] - list_proximity[$scope.device.address][i][0];
      features.date_start = new Date(list_proximity[$scope.device.address][i][0] * 1000);
      
      $scope.device.proximity.push(features);
    }
  }

  $scope.toggleGroup = function(group) 
  {
    if($scope.isGroupShown(group)) 
    {
      $scope.shownGroup = null;
    }
    else
    {
      $scope.shownGroup = group;
    }
  };

  $scope.isGroupShown = function(group) 
  {
    return $scope.shownGroup === group;
  };
});

app_sonar.config(function($stateProvider, $urlRouterProvider)
{
  $stateProvider.state('tabs',
  {
    url: "/tab",
    abstract: true,
    templateUrl: "tabs.html",
    controller: 'ctrl_tabs',
    cache: true
  });
  $stateProvider.state('tabs.home', 
  {
    url: '/home',
    views:
    {
      'home-tab':
      {
        templateUrl: 'home.html'    
        //controller: 'ctrl_home'
      }
    }
  });
  $stateProvider.state('tabs.scanner', 
  {
    url: '/scanner',
    views:
    {
      'scanner-tab':
      {
        templateUrl: 'scanner.html'    
        //controller: 'ctrl_scanner'
      }
    }
  });
  $stateProvider.state('tabs.heartbeat',
  {
    url: '/heartbeat',
    views:
    {
      'heartbeat-tab':
      {
        templateUrl: 'heartbeat.html',
        controller: 'ctrl_heartbeat'
      }
    }
  });
  $stateProvider.state('tabs.about',
  {
    url: '/about',
    views:
    {
      'about-tab':
      {
        templateUrl: 'about.html'
        //controller: 'ctrl_about'
      }
    }
  });
  $stateProvider.state('tabs.listdevice',
  {
    url: '/listdevice',
    views:
    {
      'listdevice-tab':
      {
        templateUrl: 'listdevice.html',
        controller: 'ctrl_list_device'
      }
    }
  });
  $stateProvider.state('tabs.listinteraction',
  {
    url: '/listinteraction',
    views:
    {
      'listinteraction-tab':
      {
        templateUrl: 'listinteraction.html',
        //controller: 'ctrl_list_interaction'
      }
    }
  });
  $stateProvider.state('device', 
  {
    url: '/device/:address',
    templateUrl: 'device.html',
    controller: 'ctrl_device',
    cache: false
  });
  
  $urlRouterProvider.otherwise("/tab/home");
});