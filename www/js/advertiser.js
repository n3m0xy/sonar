var uuid = "cca2e4c3-de6b-45cc-85dc-ced34ae3ddef";

function toByteArray(hexString) 
{
  var out = [];
  for (var i = 0; i < hexString.length; i += 2) 
  {
    out.push((parseInt(hexString.charAt(i), 16) << 4) + parseInt(hexString.charAt(i + 1), 16));
  }
  return out;
}

// var base64 = cordova.require('cordova/base64');
// function buildServiceData(namespace, instance) 
// {
//   //var data = [0, txPowerLevel()];
//   var data = [];
//   Array.prototype.push.apply(data, toByteArray(namespace));
//   Array.prototype.push.apply(data, toByteArray(instance));
//   return base64.fromArrayBuffer(new Uint8Array(data));
// }

//var serviceData = buildServiceData("Sonar", "Device111");
//console.log(serviceData);

var transmitData = {serviceUUIDs: [uuid]};
var settings =  {broadcastData: transmitData};

var advertiser = function()
{
    console.log("startAdvertise okay");
}

var error_advertiser = function(errorCode)
{
    console.log("startAdvertise error: " + errorCode);
}
