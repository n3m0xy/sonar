var exec = cordova.require('cordova/exec');

var active_scan = 0;
var nb_process_writing = 0;
var filename_csv = "records_scan";
var i_db = 1;

// Ranged between 2 and 4
var factor_environnment = 2;
/* Tx Power Level Transposition table : */
var txtable = [-30, -20, -16, -12, -8, -4, 0, 4];


var list_devices_rssi = [];
var listen_rssi = {};
var device_information = {};

var device_proximity = {};
//var min_distance_proximity = 2;
var timestamp_start = 0;
var timestamp_end = 0;

var time_pauseScan = 5;
var timestamp_shift = 0;
var nb_scan = 0;
var max_nb_scan = 15;
var time_record = 0;
var max_time_record = 0;
var time_proximity = 0;
var max_time_proximity = 0; 

var list_proximity = {};
var time_trigger_interaction = 20;
var max_time_elapse = 0;

//var server = "https://81.67.129.9:5000";
var server = "https://11.1.189.17:5000";
// scope of listdevice's controller
var scope_list_device;

// ###########################UTILITY FUNCTIONS################################# //

var b64ToUint6 = function(nChr) 
{
    return nChr > 64 && nChr < 91 ?
            nChr - 65
        : nChr > 96 && nChr < 123 ?
            nChr - 71
        : nChr > 47 && nChr < 58 ?
            nChr + 4
        : nChr === 43 ?
            62
        : nChr === 47 ?
            63
        :
            0;
}

var base64DecToArr = function(sBase64, nBlocksSize) 
{
    var sB64Enc = sBase64.replace(/[^A-Za-z0-9\+\/]/g, "");
    var nInLen = sB64Enc.length;
    var nOutLen = nBlocksSize ?
        Math.ceil((nInLen * 3 + 1 >> 2) / nBlocksSize) * nBlocksSize
        : nInLen * 3 + 1 >> 2;
    var taBytes = new Uint8Array(nOutLen);

    for (var nMod3, nMod4, nUint24 = 0, nOutIdx = 0, nInIdx = 0; nInIdx < nInLen; nInIdx++) {
        nMod4 = nInIdx & 3;
        nUint24 |= b64ToUint6(sB64Enc.charCodeAt(nInIdx)) << 18 - 6 * nMod4;
        if (nMod4 === 3 || nInLen - nInIdx === 1) {
            for (nMod3 = 0; nMod3 < 3 && nOutIdx < nOutLen; nMod3++, nOutIdx++) {
                taBytes[nOutIdx] = nUint24 >>> (16 >>> nMod3 & 24) & 255;
            }
            nUint24 = 0;
        }
    }

    return taBytes;
}

var littleEndianToInt8 = function(data, offset)
{
    var x = littleEndianToUint8(data, offset)
    if (x & 0x80) x = x - 256
    return x
}

function littleEndianToUint8(data, offset)
{
    return data[offset]
}

function mean(numbers)
{
    var total = 0, i;
    for(i = 0; i < numbers.length; i++) 
    {
        total += numbers[i];
    }
    return round(total / numbers.length, 2);
}

function round(value, decimals) 
{
    return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
}

function isEmpty(obj) 
{
    for(var key in obj) 
    {
        if(obj.hasOwnProperty(key))
        {
            return false;
        }
    }

    return true;
}

function arrayToUUID(array, offset)
{
    var k=0;
    var string = '';
    var UUID_format = [4, 2, 2, 2, 6];
    for (var l=0; l<UUID_format.length; l++)
    {
        if (l != 0)
        {
            string += '-';
        }
        for (var j=0; j<UUID_format[l]; j++, k++)
        {
            string += toHexString(array[offset+k], 1);
        }
    }
    return string;
}

function toHexString(i, byteCount) 
{
    var string = (new Number(i)).toString(16);
    while(string.length < byteCount*2) {
        string = '0'+string;
    }
    return string;
}

// ##################################################################### //

function display_devices(list_proximity)
{
    var address;

    scope_list_device.list_device_sonar = [];
    scope_list_device.list_device_normal = [];

    for(address in list_proximity)
    {
        if(device_information[address]["is_sonar"])
        {
            scope_list_device.list_device_sonar.push({"address": address});
        }
        else
        {
            scope_list_device.list_device_normal.push({"address": address});
        }        
    }

    scope_list_device.$apply();
}

function send_data()
{
    // Not currently writing on the file 
    if(nb_process_writing == 0)
    {
        scope_list_device.list_device_sonar = [];
        scope_list_device.list_device_normal = [];

        read_csv_and_send(filename_csv);

        for(address in list_proximity)
        {
            delete list_proximity[address];
        }

        i_db++;
    }
}

function send_data_server(list_data)
{
    var api_conn, i, address, post;

    api_conn = new XMLHttpRequest();
    api_conn.open("POST", server + "/api/scan/new/db_" + i_db + ".db");
    api_conn.send();

    setTimeout(function()
    {
        for(i in list_data)
        {
            //console.log(server + "/api/scan/" + list_data[i]);
            
            api_conn = new XMLHttpRequest();
            api_conn.open("POST", server + "/api/scan/" + list_data[i]);
            //api_conn.setRequestHeader("Content-Security-Policy", "connect-src " + server);
            api_conn.send();
            //console.log(api_conn.status);
        }
    }, 2000);
}

// function send_data(list_proximity)
// {
//     var api_conn, i, address, post;

//     for(address in list_proximity)
//     {
//         i = list_proximity[address].length - 1;

//         post = address + "/" + list_proximity[address][i][0] + "/" + list_proximity[address][i][1] + "/" + list_proximity[address][i][2] + "/" + list_proximity[address][i][3] + "/" + list_proximity[address][i][4];
//         console.log(server + "/api/scan/" + post);
        
//         api_conn = new XMLHttpRequest();
//         api_conn.open("POST", server + "/api/scan/" + post);
//         //api_conn.setRequestHeader("Content-Security-Policy", "connect-src " + server);
//         api_conn.send();
//         console.log(api_conn.responseText);
//     }   
// }

function read_csv_and_send(filename)
{
    window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function(dir)
    {
        dir.getFile(filename + ".txt", {}, function(file) 
        {
            file.file(function(open_file)
            {
                var file_reader = new FileReader();

                file_reader.onloadend = function(evt) 
                {
                    var lines = this.result.split("\n");
                    var list_data = [];

                    for(var i in lines)
                    {
                        list_data.push(lines[i]);
                    }

                    // Last element always empty
                    list_data.pop();
                    console.log("Nb record read : " + list_data.length);
                    send_data_server(list_data);
                    //remove_file(filename_csv);
                };
                
                file_reader.readAsText(open_file);            
            });
        });
    });
}

function remove_file(filename)
{
    window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function(dir)
    {
        dir.getFile(filename + ".txt", {}, function(file)
        {
            console.log("File deleted");
            file.remove();
        }, errorCallback);
    }, errorCallback);
}

function write_csv(filename, list_proximity)
{
    window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function(dir)
    {
        dir.getFile(filename + ".txt", {create:true, exclusive:false, replace:false}, function(file) 
        {
            var i, address;
            var text = "";
            var nb_record = 0;

            for(address in list_proximity)
            {
                for(i in list_proximity[address])
                {
                    text += address + "/" + list_proximity[address][i][0] + "/" + list_proximity[address][i][1] +  "/" + list_proximity[address][i][2].join(",") + "/" + list_proximity[address][i][3].join(",") + "/" + list_proximity[address][i][4] + "\n";
                    nb_record++;
                }
            }
            console.log(file.toURL());
            console.log("Nb record written : " + nb_record);

            var data = new Blob([text], { type: 'text/plain'});

            file.createWriter(function(file_writer)
            {
                //file_writer.seek(file_writer.length);

                file_writer.onwriteend = function() 
                {
                    nb_process_writing--;
                }

                file_writer.write(data);
                nb_process_writing++;

            }, errorCallback);
        }, errorCallback);
    }, errorCallback);
}

function errorCallback(error) 
{
    console.log("ERROR : " + error.code);
}

// ##################################################################### //

var logging_device = function(device)
{
    if(active_scan == 0)
    {
        exec(null, null, 'BLE', 'stopScan', []);
        return;
    }
    
    // Calculate the distance (in meters) with device base on its rssi and twPower
    this.calculate_distance = function(rssi, txPower)
    {            
        if(txPower == -1)
        {
            txPower = -59;
        }

        /*var ratio = rssi * 1.0 / txPower;
        var distance;
        
        if (ratio < 1.0) 
        {
            distance = Math.pow(ratio,10);
        }
        else 
        {
            distance = (0.89976) * Math.pow(ratio,7.7095) + 0.111;
        }*/

        var ratio = (txPower - rssi) / (10 * factor_environnment);
        var distance = Math.pow(10, ratio);

        return distance;
    }

    this.mode_rssi = function(numbers) 
    {
        // as result can be bimodal or multi-modal the returned result is provided as an array
        var modes = [], count = [], i, number, maxIndex = 0;
     
        for(i = 0; i < numbers.length; i++) 
        {
            number = numbers[i];
            count[number] = (count[number] || 0) + 1;
            if (count[number] > maxIndex) 
            {
                maxIndex = count[number];
            }
        }
     
        for (i in count)
        {
            if (count.hasOwnProperty(i)) 
            {
                if (count[i] === maxIndex) 
                {
                    modes.push(Number(i));
                }
            }
        }

        return modes;
    }

    this.information_device = function(device)
    {
        device_information[device.address] = {};
        device_information[device.address]["name"] = device.name;
        device_information[device.address]["txPower"] = -1.0;
        device_information[device.address]["is_sonar"] = false;

        if(device.scanRecord)
        {
            var byteArray = base64DecToArr(device.scanRecord);
            var length, type, pos = 0;
            var uuid;

            while(pos < byteArray.length)
            {
                length = byteArray[pos++];
                if(length == 0)
                {
                    break;
                }
                length -= 1;
                type = byteArray[pos++];

                // TX Power Level
                if(type == 0x0a)
                {
                     device_information[device.address]["txPower"] = littleEndianToInt8(byteArray, pos);
                     //txtable[
                }
                else if (type == 0x06 || type == 0x07) // 128-bit Service Class UUIDs.
                {
                    for (var i=0; i<length; i+=16)
                    {
                        uuid = arrayToUUID(byteArray, pos + i);
                        if(uuid == "efdde34a-d3ce-dc85-cc45-6bdec3e4a2cc")
                        {
                            console.log(device.address + " : " + uuid);
                            device_information[device.address]["is_sonar"] = true;
                        }
                    }
                }

                pos += length;
            }
        }
    }

    this.information_devices_rssi = function()
    {
        var list_devices_rssi = [];
        var mean_rssi, mean_mode_rssi, address, distance, nb_detection;
    
        for(address in listen_rssi)
        {
            mean_rssi = mean(listen_rssi[address]);
            mean_mode_rssi = mean(mode_rssi(listen_rssi[address]));
            distance = this.calculate_distance(mean_mode_rssi, device_information[address]["txPower"]);
            nb_detection = listen_rssi[address].length;
            list_devices_rssi.push([mean_rssi, address, distance, nb_detection]);
        }

        return list_devices_rssi.sort();
    }

    this.record_proximity = function(list_devices_rssi)
    {
        var mean_mode_rssi, address, distance, nb_detection;

        for(var i in list_devices_rssi)
        {
            mean_mode_rssi = list_devices_rssi[i][0];
            address = list_devices_rssi[i][1];
            distance = round(list_devices_rssi[i][2], 2);
            nb_detection = list_devices_rssi[i][3];

            console.log(address + " " + mean_mode_rssi + "(" + nb_detection + ")");

            //distance <= min_distance_proximity
            //if(mean_mode_rssi > -50 && nb_detection > (time_record * max_nb_scan /2))
            if(1==1)
            {
                if(!(address in device_proximity))
                {
                    device_proximity[address] = [];
                }

                device_proximity[address].push([timestamp_start, timestamp_end, mean_mode_rssi, distance, nb_detection]);
            }
        }
    }

    this.save_proximity = function(device_proximity)
    {
        var i, j, start_i, end_i;
        var t_start, t_end;
        var proximity = {};
        var new_proximity;

        for(address in device_proximity)
        {
            //console.log(address + " has " + device_proximity[address].length + " records");
            proximity[address] = [];
            if(device_proximity[address].length == 1)
            {
                proximity[address].push([0, 0]);
                continue;
            }

            start_i = 0;
            end_i = 0;

            i = 1;
            while(i < device_proximity[address].length - 1)
            {
                time_elapse = device_proximity[address][i][0] - device_proximity[address][i-1][1];
                
                if(time_elapse > max_time_elapse)
                {
                    console.log("Proximity detected last " + (device_proximity[address][end_i][1] - device_proximity[address][start_i][0]) + ". Elapsed time : " + time_elapse);
                    proximity[address].push([start_i, end_i]);
                    start_i = i;
                }
                    
                end_i = i;
                i++;   
            }

            time_elapse = device_proximity[address][i][0] - device_proximity[address][i-1][1];

            if(time_elapse > max_time_elapse)
            {
                console.log("A] Proximity detected last " + (device_proximity[address][end_i][1] - device_proximity[address][start_i][0]) + ". Elapsed time : " + time_elapse);
                proximity[address].push([start_i, end_i]);
                proximity[address].push([i, i]);
            }
            else
            {
                console.log("B] Proximity detected last " + (device_proximity[address][i][1] - device_proximity[address][start_i][0]) + ". Elapsed time : " + time_elapse);
                proximity[address].push([start_i, i]);
            }
        }

        for(address in proximity)
        {
            if(!(address in list_proximity))
            {
                list_proximity[address] = [];    
            }

            for(i in proximity[address])
            {
                t_start = device_proximity[address][proximity[address][i][0]][0];
                t_end = device_proximity[address][proximity[address][i][1]][1];

                // console.log(address)
                // console.log(t_end - t_start);
                // if(list_proximity[address].length > 0)
                //     {
                //     console.log(t_start - list_proximity[address][list_proximity[address].length - 1][1]);
                //     }
                // console.log("-------");

                if(i == 0 && list_proximity[address].length > 0 && max_time_elapse > (t_start - list_proximity[address][list_proximity[address].length - 1][1]))
                {
                    // Update on-going proximity (t_end timestamp)
                    list_proximity[address][list_proximity[address].length - 1][1] = device_proximity[address][proximity[address][i][1]][1];                    
                }
                else
                {
                    // New proximity
                    list_proximity[address].push([t_start, t_end, [], [], 0]);
                }

                // Update information (rssi, distance, nb_detection)
                for(j = proximity[address][i][0]; j < proximity[address][i][1]+1; j++)
                {
                    //list_proximity[address][list_proximity[address].length - 1][2].push(device_proximity[address][j][2]);
                    list_proximity[address][list_proximity[address].length - 1][2].push(device_proximity[address][j][2]);
                    list_proximity[address][list_proximity[address].length - 1][3].push(device_proximity[address][j][3]);
                    list_proximity[address][list_proximity[address].length - 1][4] += device_proximity[address][j][4];
                }
            }
        }
    }

    this.log = function(device)
    {
        if(!(device.address in listen_rssi))
        {
            listen_rssi[device.address] = [];
            this.information_device(device);
        }
                
        listen_rssi[device.address].push(device.rssi);
    }
    
    this.log(device);
    
    if(nb_scan == max_nb_scan)
    {
        nb_scan = 0;

        exec(logging_device, error_logging, 'BLE', 'pauseScan', [time_pauseScan * 1000]);
        //timestamp_shift += time_pauseScan;

        if(time_record == max_time_record)
        {            
            timestamp_end = Math.floor(Date.now() / 1000);

            list_devices_rssi = this.information_devices_rssi();
            this.record_proximity(list_devices_rssi);

            console.log("scan done for " + (timestamp_end - timestamp_start));
            
            // Reset logs
            for(address in listen_rssi)
            {
                delete listen_rssi[address];
            }

            if(time_proximity == max_time_proximity)
            {
                console.log("save proximity");
                this.save_proximity(device_proximity);             
                
                for(address in device_proximity)
                {
                    delete device_proximity[address];
                }

                display_devices(list_proximity);
                //send_data(list_proximity);
                
                time_proximity = 0;
            }
            else
            {
                time_proximity++;
            }

            time_record = 0;

            timestamp_start = Math.floor(Date.now() / 1000);// + timestamp_shift;
        }
        else
        {
            time_record++;
        }
    }
    else
    {
        nb_scan++;
    }
}

var error_logging = function(errorCode) 
{
    console.log('startScan error: ' + errorCode);
}